
import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:go_router/go_router.dart';
import 'package:online_store/blocs/app/app_bloc.dart';
import 'package:online_store/blocs/home/home_bloc.dart';
import 'package:online_store/l10n/l10n.dart';
import 'package:online_store/widgets/custom_icon.dart';
import 'package:online_store/widgets/side_menu.dart';
import 'package:provider/provider.dart';

import '../blocs/app/app_event.dart';
import '../blocs/home/home_event.dart';
import '../blocs/home/home_state.dart';
import '../models/product_model.dart';
import '../utils/custom_color.dart';
import '../utils/value_manager.dart';
import '../widgets/feeds_widget.dart';
import '../widgets/language_drop_down.dart';
import '../widgets/sale_widget.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => HomeBloc()..add(LoadDataEvent()),
      child: const HomeScreen(),
    );
  }

}

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  late TextEditingController _textEditingController;

  @override
  void initState() {
    _textEditingController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: (){FocusManager.instance.primaryFocus?.unfocus();},
      child: SafeArea(
          child: Scaffold(
              appBar: AppBar(
                actions: const [
                  ThemeSwitched(),
                  SizedBox(
                    width: AppSize.s10,
                  ),
                  LanguageDropDown(),
                ],
              ),
              body: Row(
                children: [
                  size.width > webScreenSize ? const SizedBox(
                    width: sideMenuSize,
                    height: double.infinity,
                    child: SideMenu(),
                  ) : const SizedBox(),
                  AnimatedContainer(
                    width: size.width > webScreenSize
                        ? size.width - sideMenuSize
                        : size.width,
                    duration: const Duration(milliseconds: AppDuration.d200),
                    child: Padding(
                      padding: const EdgeInsets.all(AppPadding.p8),
                      child: Column(
                        // crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Center(
                            child: TextField(
                              controller: _textEditingController,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                  hintText: context.l10n.search,
                                  filled: true,
                                  fillColor: Theme.of(context).cardColor,
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.circular(AppSize.s10),
                                    borderSide: BorderSide(
                                      color: Theme.of(context).cardColor,
                                    ),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.circular(AppSize.s10),
                                    borderSide: BorderSide(
                                      width: 1,
                                      color:
                                          Theme.of(context).colorScheme.secondary,
                                    ),
                                  ),
                                  suffixIcon: InkWell(
                                    child: Icon(
                                      IconlyLight.search,
                                      color: lightIconsColor,
                                    ),
                                    onTap: () {
                                      context.pushNamed("errorPage");
                                    },
                                  )),
                            ),
                          ),
                          const SizedBox(
                            height: AppSize.s10,
                          ),
                          Expanded(
                              child: SingleChildScrollView(
                            child: Column(
                              children: [
                                SizedBox(
                                  height: size.height * 0.4,
                                  child: Swiper(
                                    itemCount: 3,
                                    itemBuilder: (ctx, index) {
                                      return const SaleWidget();
                                    },
                                    autoplay: true,
                                    pagination: const SwiperPagination(
                                        alignment: Alignment.bottomCenter,
                                        builder: DotSwiperPaginationBuilder(
                                            color: Colors.white,
                                            activeColor: Colors.red)),
                                    // control: const SwiperControl(),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(AppPadding.p8),
                                  child: Row(
                                    children: [
                                      Text(
                                        context.l10n.latestProduct,
                                        style: const TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: AppFontSize.fs18),
                                      ),
                                      const Spacer(),
                                      IconButton(
                                        onPressed: () {
                                          FocusManager.instance.primaryFocus?.unfocus();
                                          context.pushNamed('productsPage', params: {'endpoint' : 'AllProduct',});
                                        },
                                        icon: const CustomIcon(
                                            ltrIcon: IconlyBold.arrowRight2,
                                            rtlIcon: IconlyBold.arrowLeft2),
                                      )
                                    ],
                                  ),
                                ),
                                BlocBuilder<HomeBloc, HomeState>(
                                  builder: (_, state) {
                                    if(state.runtimeType == HomeStateLoading) {
                                      return const Center(
                                        child: CircularProgressIndicator(),
                                      );
                                    }else if(state.runtimeType == HomeStateLoadingFail) {
                                      return Column(
                                        children: [
                                          Text(context.l10n.dataLoadFailed),
                                          const SizedBox(height: AppSize.s10,),
                                          IconButton(onPressed: (){context.read<HomeBloc>().add(LoadDataEvent());}, icon: const Icon(Icons.refresh),)
                                        ]
                                      );
                                    }else {
                                      return feedsGridWidget(state.productList);
                                    }
                                  },
                                )
                              ],
                            ),
                          ))
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              drawer: size.width < webScreenSize
                  ? GestureDetector(
                child: const Drawer(
                  child: SideMenu(),
                ),
              )
                  : null)),
    );
  }

  Widget feedsGridWidget(final List<ProductModel> productList) {
    return GridView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: 5,
        gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 300,
            crossAxisSpacing: AppSize.s10,
            mainAxisSpacing: AppSize.s10,
            childAspectRatio: 0.6),
        itemBuilder: (context, index) {
          return Provider.value(
            value: productList[index],
            child: const FeedsWidget(),
          );
        });
  }

}

class ThemeSwitched extends StatelessWidget {
  const ThemeSwitched({super.key});

  @override
  Widget build(BuildContext context) {
    final theme = context.select((AppBloc b) => b.state.themeState.name);
    bool isSwitched = theme == 'light' ? false : theme == 'dark' ? true : MediaQuery.maybeOf(context)?.platformBrightness == Brightness.light ? false : true;
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Icon(Icons.sunny),
        Switch(
          value: isSwitched,
          onChanged: (_) {context.read<AppBloc>().add(ThemeChanged());},
          activeColor: Colors.deepOrange,
        ),
        const Icon(Icons.shield_moon)
      ],
    );
  }

}
