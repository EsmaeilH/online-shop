
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:online_store/blocs/categories_cubit/categories_cubit.dart';
import 'package:online_store/blocs/categories_cubit/categories_state.dart';
import 'package:online_store/l10n/l10n.dart';
import 'package:online_store/models/Categories_model.dart';
import 'package:online_store/widgets/category_widget.dart';
import 'package:provider/provider.dart';

import '../utils/value_manager.dart';
import '../widgets/side_menu.dart';

class CategoriesPage extends StatelessWidget {
  const CategoriesPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => CategoriesCubit()..onCategoriesLoad(),
      child: const CategoriesScreen(),
    );
  }

}

class CategoriesScreen extends StatelessWidget {
  const CategoriesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(context.l10n.categories),
          automaticallyImplyLeading: size.width >= webScreenSize ? false : true,
        ),
        body: Row(
          children: [
            size.width > webScreenSize ? const SizedBox(
              width: sideMenuSize,
              height: double.infinity,
              child: SideMenu(),
            ) : const SizedBox(),
            AnimatedContainer(
              width: size.width > webScreenSize
                  ? size.width - sideMenuSize
                  : size.width,
              duration: const Duration(milliseconds: AppDuration.d200),
              child: BlocBuilder<CategoriesCubit, CategoriesState>(
                  builder: (context, state) {
                    if(state.runtimeType == CategoriesLoading) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    }else if(state.runtimeType == CategoriesFailed) {
                      return Center(
                        child: Column(
                            children: [
                              Text(context.l10n.dataLoadFailed),
                              const SizedBox(height: AppSize.s10,),
                              IconButton(onPressed: (){}, icon: const Icon(Icons.refresh),)
                            ]
                        ),
                      );
                    }else {
                      return SingleChildScrollView(
                        child: feedsGridWidget(state.categoriesList),
                      );
                    }
                  }
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget feedsGridWidget(final List<CategoriesModel> categoriesList) {
    return GridView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: categoriesList.length,
        gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 300,
            crossAxisSpacing: AppSize.s10,
            mainAxisSpacing: AppSize.s10,
            childAspectRatio: 0.6),
        itemBuilder: (context, index) {
          return Provider.value(
            value: categoriesList[index],
            child: const CategoryWidget(),
          );
        });
  }

}