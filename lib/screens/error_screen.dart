
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:online_store/l10n/l10n.dart';

import '../utils/value_manager.dart';

class ErrorScreen extends StatefulWidget {
  const ErrorScreen({super.key});

  @override
  State<StatefulWidget> createState() => ErrorScreenState();
}

class ErrorScreenState extends State<ErrorScreen> with SingleTickerProviderStateMixin{
  late AnimationController _animationController;
  late Tween<double> scaleTween;
  late Animation<double> scaleAnimation;

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: AppDuration.ds2)
    )..repeat(reverse: false);
    scaleTween = Tween(
      begin: 1,
      end: 5
    );
    scaleAnimation = _animationController.drive(scaleTween);
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: kIsWeb ? false : true,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AnimatedBuilder(
                animation: _animationController,
                builder: (BuildContext context, child) {
                  return Transform.scale(
                    scale: scaleAnimation.value,
                    child: child
                  );
                },
                child: const Icon(IconlyBold.danger),
              ),
              const SizedBox(height: AppSize.s50,),
              Text(context.l10n.pageNotFound, style: const TextStyle(fontSize: AppSize.s20, fontWeight: FontWeight.w700),)
            ],
          ),
        ),
      )
    );
  }

}