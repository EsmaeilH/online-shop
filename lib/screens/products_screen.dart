import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:online_store/blocs/products/products_bloc.dart';
import '../blocs/home/home_bloc.dart';
import '../blocs/products/products_event.dart'; /////
import 'package:online_store/blocs/products/products_state.dart';
import 'package:online_store/l10n/l10n.dart';
import 'package:provider/provider.dart';

import '../models/product_model.dart';
import '../utils/value_manager.dart';
import '../widgets/feeds_widget.dart';
import '../widgets/side_menu.dart';

class ProductsPage extends StatelessWidget {
  const ProductsPage(
      {super.key, required this.endpoint});

  final String endpoint;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        BlocProvider(
          create: (_) =>
              ProductsBloc()..add(ProductsFetchEvent(limitNumber: 10)),
        ),
        Provider.value(value: endpoint),
      ],
      child: const ProductsScreen(),
    );
  }
}

class ProductsScreen extends StatefulWidget {
  const ProductsScreen({super.key});

  @override
  State<StatefulWidget> createState() => ProductsScreenState();
}

class ProductsScreenState extends State<ProductsScreen> {
  final ScrollController _scrollController = ScrollController();

  @override
  void didChangeDependencies() {
    _scrollController.addListener(() async {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        final limit = 10 + context.read<ProductsBloc>().state.loadLimitNumber;
        context.read<ProductsBloc>().add(ProductsFetchMore(limitNumber: limit));
      }
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(context.l10n.allProducts),
          automaticallyImplyLeading: size.width >= webScreenSize ? false : true,
        ),
        body: Row(
          children: [
            size.width > webScreenSize
                ? const SizedBox(
                    width: sideMenuSize,
                    height: double.infinity,
                    child: SideMenu(),
                  )
                : const SizedBox(),
            AnimatedContainer(
              width: size.width > webScreenSize
                  ? size.width - sideMenuSize
                  : size.width,
              duration: const Duration(milliseconds: AppDuration.d200),
              child: BlocBuilder<ProductsBloc, ProductsState>(
                  builder: (context, state) {
                if (state.runtimeType == ProductsLoading) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                } else if (state.runtimeType == ProductsLoadingFailed) {
                  return Center(
                    child: Column(children: [
                      Text(context.l10n.dataLoadFailed),
                      const SizedBox(
                        height: AppSize.s10,
                      ),
                      IconButton(
                        onPressed: () {},
                        icon: const Icon(Icons.refresh),
                      )
                    ]),
                  );
                } else {
                  return SingleChildScrollView(
                    controller: _scrollController,
                    child: Column(
                      children: [
                        feedsGridWidget(state.productList),
                        const SizedBox(
                          height: AppSize.s10,
                        ),
                        state.isLoading
                            ? const CircularProgressIndicator()
                            : const SizedBox(),
                        const SizedBox(
                          height: AppSize.s10,
                        )
                      ],
                    ),
                  );
                }
              }),
            )
          ],
        ),
      ),
    );
  }

  Widget feedsGridWidget(final List<ProductModel> productList) {
    return GridView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: productList.length,
        gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 300,
            crossAxisSpacing: AppSize.s10,
            mainAxisSpacing: AppSize.s10,
            childAspectRatio: 0.6),
        itemBuilder: (context, index) {
          return Provider.value(
            value: productList[index],
            child: const FeedsWidget(),
          );
        });
  }
}
