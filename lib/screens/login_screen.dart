
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:online_store/blocs/app/app_bloc.dart';
import 'package:online_store/blocs/app/app_event.dart';
import 'package:online_store/blocs/login/login_bloc.dart';
import 'package:online_store/l10n/l10n.dart';
import 'package:online_store/services/database_handler.dart';
import 'package:online_store/utils/value_manager.dart';
import 'package:flutter/foundation.dart';

import '../blocs/login/login_event.dart';
import '../blocs/login/login_state.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
   return BlocProvider(
     create: (_) => LoginBloc(stateDao: context.read<AppStateDao>()),
     child: const LoginScreen(),
   );
  }

}

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        appBar: !kIsWeb ? AppBar(
        ) : null,
        body: BlocListener<LoginBloc, LoginState>(
          listener: (BuildContext context, state) {
            if(state.runtimeType == LoginSuccess) {
              context.read<AppBloc>().add(AuthStateChanged(isLogin: true));
              context.goNamed('home');
            }
          },
          child: AnimatedContainer(
            duration: const Duration(milliseconds: AppDuration.d300),
            padding: EdgeInsets.symmetric(vertical: AppPadding.p20, horizontal: size.width < 500 ? 0 : AppPadding.p20),
            child: Center(
              child: Container(
                decoration: const BoxDecoration(
                    color: Colors.black12
                ),
                constraints: const BoxConstraints(
                    maxWidth: 500
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    const SizedBox(height: AppSize.s20,),
                    Text(context.l10n.login),
                    const Spacer(),
                    Padding(
                      padding: const EdgeInsets.all(AppPadding.p8),
                      child: TextField(
                        textDirection: TextDirection.ltr,
                        controller: _emailController,
                        decoration: const InputDecoration(
                          hintTextDirection: TextDirection.ltr,
                          hintText: 'email'
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(AppPadding.p8),
                      child: TextFormField(
                        textDirection: TextDirection.ltr,
                        controller: _passwordController,
                        obscureText: true,
                        decoration: const InputDecoration(
                          hintTextDirection: TextDirection.ltr,
                          hintText: 'password'
                        ),
                        onFieldSubmitted: (_) {
                          if(_emailController.text.isNotEmpty && _passwordController.text.isNotEmpty) {
                            context.read<LoginBloc>().add(LoginEvent(email: _emailController.text, password: _passwordController.text));
                          }
                        },
                      ),
                    ),
                    const SizedBox(height: AppSize.s10,),
                    ElevatedButton(
                      onPressed: () {
                        if(_emailController.text.isNotEmpty && _passwordController.text.isNotEmpty) {
                          context.read<LoginBloc>().add(LoginEvent(email: _emailController.text, password: _passwordController.text));
                        }
                      },
                      child: Text(context.l10n.login),
                    ),
                    const SizedBox(height: AppSize.s10,),
                    context.watch<LoginBloc>().state.runtimeType == LoginProcessing ?
                    const CircularProgressIndicator()
                        : context.watch<LoginBloc>().state.runtimeType == LoginFailed ?
                    const Text("Failed", style: TextStyle(color: Colors.red),)
                        : const SizedBox(),
                    const Spacer()
                  ],
                ),
              ),
            ),
          ),
        )
      )
    );
  }

}