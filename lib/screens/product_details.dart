
import 'package:card_swiper/card_swiper.dart';
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:online_store/blocs/product-cubit/product_cubit.dart';
import 'package:online_store/blocs/product-cubit/product_state.dart';
import 'package:online_store/l10n/l10n.dart';

import '../blocs/app/app_bloc.dart';
import '../models/product_model.dart';
import '../utils/custom_color.dart';
import '../utils/value_manager.dart';
import '../widgets/side_menu.dart';

class ProductDetailsPage extends StatelessWidget {
  final String id;
  const ProductDetailsPage({super.key, required this.id});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => ProductCubit()..onProductLoad(id),
      child: const ProductDetailsScreen(),
    );
  }

}

class ProductDetailsScreen extends StatelessWidget {
  const ProductDetailsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: kIsWeb ? false : true,
        ),
        body: Row(
          children: [
            size.width > webScreenSize ? const SizedBox(
              width: sideMenuSize,
              height: double.infinity,
              child: SideMenu(),
            ) : const SizedBox(),
            AnimatedContainer(
              width: size.width > webScreenSize
                  ? size.width - sideMenuSize
                  : size.width,
              duration: const Duration(milliseconds: AppDuration.d200),
              child: BlocBuilder<ProductCubit, ProductState>(
                  builder: (context, state) {
                    if(state.runtimeType == ProductLoading) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    }else if(state.runtimeType == ProductLoadFailed) {
                      return Center(
                        child: Column(
                            children: [
                              Text(context.l10n.dataLoadFailed),
                              const SizedBox(height: AppSize.s10,),
                              IconButton(onPressed: (){}, icon: const Icon(Icons.refresh),)
                            ]
                        ),
                      );
                    }else {
                      return productDetails(state.product!);
                    }
                  }
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget productDetails(ProductModel product) {
    const titleStyle = TextStyle(fontSize: AppFontSize.fs24, fontWeight: FontWeight.bold);
    return LayoutBuilder(
      builder: (context, constrain) {
        final theme = context.read<AppBloc>().state.themeState;
        return SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(AppPadding.p8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      product.category!.name.toString(),
                      style: const TextStyle(
                          fontSize: AppFontSize.fs18, fontWeight: FontWeight.w500),
                    ),
                    const SizedBox(
                      height: AppSize.s18,
                    ),
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          flex: 3,
                          child: Text(
                            product.title.toString(),
                            textAlign: TextAlign.start,
                            style: titleStyle,
                          ),
                        ),
                        Flexible(
                          child: RichText(
                            text: TextSpan(
                                text: '\$',
                                style: const TextStyle(
                                    fontSize: AppFontSize.fs24,
                                    color: Color.fromRGBO(
                                        33, 150, 243, 1)),
                                children: <TextSpan>[
                                  TextSpan(
                                      text: product.price
                                          .toString(),
                                      style: TextStyle(
                                          color: theme.name ==
                                              'light' ? lightTextColor : theme.name == 'dark' ? darkTextColor
                                              : MediaQuery.maybeOf(context)?.platformBrightness == Brightness.light ? lightTextColor : darkTextColor,
                                          fontWeight:
                                          FontWeight.bold)),
                                ]),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: AppSize.s18,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.4,
                child: Swiper(
                  itemBuilder: (context, index) {
                    return FancyShimmerImage(
                      width: double.infinity,
                      imageUrl: product.images![index].toString(),
                      errorWidget: const Center(
                        child: Icon(IconlyBold.danger),
                      ),
                    );
                  },
                  autoplay: true,
                  itemCount: product.images!.length,
                  pagination: const SwiperPagination(
                      alignment: Alignment.bottomCenter,
                      builder: DotSwiperPaginationBuilder(
                          color: Colors.white,
                          activeColor: Colors.red
                      )
                  ),
                ),
              ),
              const SizedBox(
                height: AppSize.s18,
              ),
              Padding(
                padding: const EdgeInsets.all(AppPadding.p8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text('Description', style: titleStyle),
                    const SizedBox(
                      height: AppSize.s18,
                    ),
                    Text(
                      product.description.toString(),
                      textAlign: TextAlign.start,
                      style: const TextStyle(fontSize: AppFontSize.fs24),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      }
    );
  }

}

