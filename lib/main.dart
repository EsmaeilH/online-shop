import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:online_store/blocs/app/app_bloc.dart';
// ignore: depend_on_referenced_packages
import 'package:flutter_web_plugins/url_strategy.dart';
import 'package:online_store/services/database_handler.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

import 'app.dart';
import 'models/app_state_model.dart';

void main() async{
  try{
    usePathUrlStrategy();
    WidgetsFlutterBinding.ensureInitialized();

    if(!kIsWeb) {
      final applicationDocumentDir =
      await path_provider.getApplicationDocumentsDirectory();
      Hive.init(applicationDocumentDir.path);
    }else {
      await Hive.initFlutter();
    }

    Hive.registerAdapter(AppStateModelAdapter());
    Box box = await Hive.openBox('appStateBox');
    runApp(
        RepositoryProvider<AppStateDao>(
          create: (_) => AppStateDao(dataBox: box),
          child: const AppState(),
        )
    );
  }catch(error){
    log("an error occurred while starting the app $error");
  }

}

class AppState extends StatelessWidget {
  const AppState({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => AppBloc(stateDao: context.read<AppStateDao>()),
      child: const App(),
    );
  }

}