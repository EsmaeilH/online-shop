
abstract class HomeEvent {}

class LoadDataEvent extends HomeEvent{}

class LoadTimeoutEvent extends HomeEvent{}