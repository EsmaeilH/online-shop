import 'package:bloc/bloc.dart';
import 'package:online_store/blocs/home/home_state.dart';
import 'package:online_store/services/api_handler.dart';

import 'home_event.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(const HomeStateLoading()) {
    on<LoadDataEvent>(_loadDataEvent);
    on<LoadTimeoutEvent>(_loadTimeoutEvent);
  }

  void _loadDataEvent(LoadDataEvent event, Emitter<HomeState> emit) async{
    emit(const HomeStateLoading());
    try {
      final data = await APIHandler.getAllProducts(limit: '5');
      emit(HomeStateLoadingSuccess(productList: data));
    } catch (error) {
      emit(const HomeStateLoadingFail());
    }
  }

  void _loadTimeoutEvent(LoadTimeoutEvent event, Emitter<HomeState> emit) {
    emit(const HomeStateLoadingFail());
  }
}