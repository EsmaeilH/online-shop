
import 'package:equatable/equatable.dart';
import 'package:online_store/models/product_model.dart';

abstract class HomeState extends Equatable {
  final List<ProductModel> productList;
  const HomeState({this.productList = const []});

  @override
  // TODO: implement props
  List<Object?> get props => [productList];
}

class HomeStateLoading extends HomeState {
  const HomeStateLoading();
}

class HomeStateLoadingSuccess extends HomeState {
  const HomeStateLoadingSuccess({required super.productList});
}

class HomeStateLoadingFail extends HomeState {
  const HomeStateLoadingFail();
}