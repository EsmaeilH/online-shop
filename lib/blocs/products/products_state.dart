import 'package:equatable/equatable.dart';

import '../../models/product_model.dart';

class ProductsState extends Equatable {
  final List<ProductModel> productList;
  final int loadLimitNumber;
  final bool isLoading;

  const ProductsState(
      {this.productList = const [],
      this.loadLimitNumber = 10,
      this.isLoading = false});

  ProductsState copyWith({List<ProductModel>? productList, int? loadLimitNumber,
    bool? isLoading}) =>
      ProductsState(
          productList: productList ?? this.productList,
          loadLimitNumber: loadLimitNumber ?? this.loadLimitNumber,
          isLoading: isLoading ?? this.isLoading);

  @override
  // TODO: implement props
  List<Object?> get props => [productList, loadLimitNumber, isLoading];
}

class ProductsLoading extends ProductsState {}

class ProductsLoadingFailed extends ProductsState {}

class ProductsLoaded extends ProductsState {
  const ProductsLoaded(
      {required super.productList,
      required super.loadLimitNumber,
      required super.isLoading});
}
