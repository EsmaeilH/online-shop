
import 'package:bloc/bloc.dart';
import 'package:online_store/blocs/products/products_event.dart';
import 'package:online_store/blocs/products/products_state.dart';
import 'package:online_store/services/api_handler.dart';

class ProductsBloc extends Bloc<ProductsEvent, ProductsState> {
  ProductsBloc() : super(const ProductsState()) {
    on<ProductsFetchEvent>(_onProductsFetchEvent);
    on<ProductsFetchMore>(_onProductsFetchMore);
  }


  void _onProductsFetchEvent(ProductsFetchEvent event, Emitter<ProductsState> emit) async{
    emit(ProductsLoading());
    try {
      final data = await APIHandler.getAllProducts(limit: event.limitNumber.toString());
      emit(ProductsLoaded(productList: data, loadLimitNumber: event.limitNumber, isLoading: false));
    } catch(error) {
      emit(ProductsLoadingFailed());
    }
  }

  void _onProductsFetchMore(ProductsFetchMore event, Emitter<ProductsState> emit) async{
    emit(state.copyWith(isLoading: true));
    try {
      final data = await APIHandler.getAllProducts(limit: event.limitNumber.toString());
      emit(ProductsLoaded(productList: data, loadLimitNumber: event.limitNumber, isLoading: false));
    } catch(error) {
      emit(state.copyWith(isLoading: false));
    }
  }
}