
abstract class ProductsEvent {}

class ProductsFetchEvent extends ProductsEvent {
  final int limitNumber;
  ProductsFetchEvent({required this.limitNumber});
}

class ProductsFetchMore extends ProductsEvent {
  final int limitNumber;
  ProductsFetchMore({required this.limitNumber});
}