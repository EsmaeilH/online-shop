
import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:online_store/blocs/login/login_event.dart';
import 'package:online_store/blocs/login/login_state.dart';
import 'package:online_store/models/app_state_model.dart';
import 'package:online_store/services/api_handler.dart';

import '../../services/database_handler.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc({required this.stateDao}) : super(LoginInitial()) {
    on<LoginEvent>(_onLoginEvent);
  }

  final AppStateDao stateDao;

  FutureOr<void> _onLoginEvent(LoginEvent event, Emitter<LoginState> emit) async {
    emit(LoginProcessing());
    try {
      final data = await APIHandler.login(email: event.email, password: event.password);
      AppStateModel stateModel = AppStateModel(theme: stateDao.defaultModel.theme, locale: stateDao.defaultModel.locale, accessToken: data[ "access_token"]);
      stateDao.update(stateModel);
      emit(LoginSuccess());
    } catch (error) {
      emit(LoginFailed());
    }
  }
}