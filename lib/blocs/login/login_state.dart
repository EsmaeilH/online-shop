import 'package:equatable/equatable.dart';

abstract class LoginState extends Equatable {
  @override
  List<Object?> get props => [];
}

class LoginInitial extends LoginState {}

class LoginProcessing extends LoginState {}

class LoginSuccess extends LoginState {}

class LoginFailed extends LoginState {}
