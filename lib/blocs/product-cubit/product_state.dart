
import 'package:equatable/equatable.dart';
import 'package:online_store/models/product_model.dart';

class ProductState extends Equatable {
  final ProductModel? product;

  const ProductState({this.product});

  @override
  // TODO: implement props
  List<Object?> get props => [product];
}

class ProductLoading extends ProductState {}

class ProductLoaded extends ProductState {
  const ProductLoaded({required super.product});
}

class ProductLoadFailed extends ProductState{}