import 'package:bloc/bloc.dart';
import 'package:online_store/blocs/product-cubit/product_state.dart';
import 'package:online_store/services/api_handler.dart';

class ProductCubit extends Cubit<ProductState> {
  ProductCubit() : super(const ProductState());

  void onProductLoad(String id) async => emit(await _onProductLoad(id));

  Future<ProductState> _onProductLoad(String id) async {
    emit(ProductLoading());

    try {
      final data = await APIHandler.getProductById(id: id);
      return ProductLoaded(product: data);
    } catch(error) {
      return ProductLoadFailed();
    }
  }
}
