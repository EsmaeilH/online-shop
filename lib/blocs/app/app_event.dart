
abstract class AppEvent {}

class ThemeChanged extends AppEvent {}

class LocaleChanged extends AppEvent {
  LocaleChanged({required this.locale});
  final String locale;
}

class AuthStateChanged extends AppEvent {
  AuthStateChanged({required this.isLogin});
  final bool isLogin;
}

class InitialFetch extends AppEvent {}

class FetchProfile extends AppEvent {
  FetchProfile({required this.token});
  final String token;
}