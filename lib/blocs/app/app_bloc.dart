import 'package:bloc/bloc.dart';
import 'package:online_store/blocs/app/app_event.dart';
import 'package:online_store/blocs/app/app_state.dart';
import 'package:online_store/models/app_state_model.dart';

import '../../services/api_handler.dart';
import '../../services/database_handler.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  final AppStateDao stateDao;

  AppBloc({required this.stateDao})
      : super(AppState(
          themeState: stateDao.defaultModel.theme == 'light'
              ? ThemeState.light
              : stateDao.defaultModel.theme == 'dark'
                  ? ThemeState.dark
                  : ThemeState.system,
          localeState: stateDao.defaultModel.locale == 'en'
              ? LocaleState.en
              : LocaleState.fa,
          loginState: stateDao.defaultModel.accessToken != ''
              ? AuthState.authenticated
              : AuthState.unauthenticated,
        )) {
    on<ThemeChanged>(_onThemeChanged);
    on<LocaleChanged>(_onLocaleChanged);
    on<AuthStateChanged>(_onAuthStateChanged);
    on<FetchProfile>(_onFetchProfile);
  }

  void _onThemeChanged(ThemeChanged event, Emitter<AppState> emit) {
    ThemeState themeState = state.themeState == ThemeState.light
        ? ThemeState.dark
        : ThemeState.light;

    AppStateModel appStateModel = AppStateModel(
      theme: themeState.name,
      locale: state.localeState.name,
      accessToken: stateDao.defaultModel.accessToken,
    );
    stateDao.update(appStateModel);
    emit(state.copyWith(themeState: themeState));
  }

  void _onLocaleChanged(LocaleChanged event, Emitter<AppState> emit) {
    for (var item in LocaleState.values) {
      if (item.name == event.locale) {
        AppStateModel appStateModel = AppStateModel(
          theme: state.themeState.name,
          locale: item.name,
          accessToken: stateDao.defaultModel.accessToken,
        );
        stateDao.update(appStateModel);
        emit(state.copyWith(localeState: item));
      }
    }
  }

  void _onAuthStateChanged(AuthStateChanged event, Emitter<AppState> emit) {
    AuthState authState =
        event.isLogin ? AuthState.authenticated : AuthState.unauthenticated;
    if (authState == AuthState.unauthenticated) {
      AppStateModel appStateModel = AppStateModel(
        theme: state.themeState.name,
        locale: state.localeState.name,
        accessToken: '',
      );
      stateDao.update(appStateModel);
    }
    AppState newState = state.copyWith(loginState: authState);
    emit(newState);
  }

  void _onFetchProfile(FetchProfile event, Emitter<AppState> emit) async {
    try {
      final data = await APIHandler.getUserProfile(token: event.token);
      final newState = state.copyWith(model: data);
      emit(newState);
    } catch (error) {
      emit(state);
    }
  }
}
