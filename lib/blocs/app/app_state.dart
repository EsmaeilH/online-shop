import 'package:equatable/equatable.dart';
import 'package:online_store/models/user_model.dart';

enum AuthState { unknown, unauthenticated, authenticated }

enum ThemeState { light, dark, system }

enum LocaleState { en, fa }

class AppState extends Equatable {
  const AppState(
      {required this.loginState,
      required this.themeState,
      required this.localeState,
      this.model});

  final AuthState loginState;
  final ThemeState themeState;
  final LocaleState localeState;
  final UserModel? model;

  AppState copyWith(
      {AuthState? loginState,
      ThemeState? themeState,
      LocaleState? localeState,
      UserModel? model}) {
    return AppState(
        loginState: loginState ?? this.loginState,
        themeState: themeState ?? this.themeState,
        localeState: localeState ?? this.localeState,
        model: model ?? this.model);
  }


  @override
  toString() {
    return 'loginState: $localeState, themeState: $themeState, loginState: $loginState, model: $model}';
  }

  @override
  // TODO: implement props
  List<Object?> get props => [loginState, themeState, localeState, model];
}