
import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:online_store/services/api_handler.dart';

import 'categories_state.dart';

class CategoriesCubit extends Cubit<CategoriesState> {
  CategoriesCubit() : super(const CategoriesState());

  void onCategoriesLoad() async=> emit(await _onLoadStateChange());

  Future<CategoriesState> _onLoadStateChange() async{
    emit(CategoriesLoading());

    try {
      final data = await APIHandler.getAllCategories();
      return CategoriesSuccess(categoriesList: data);
    } catch(error) {
      return CategoriesFailed();
    }
  }

}