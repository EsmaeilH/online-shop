
import 'package:equatable/equatable.dart';
import 'package:online_store/models/Categories_model.dart';

class CategoriesState extends Equatable {
  const CategoriesState({this.categoriesList = const []});
  final List<CategoriesModel> categoriesList;

  @override
  // TODO: implement props
  List<Object?> get props => [categoriesList];
}

class CategoriesLoading extends CategoriesState {}

class CategoriesSuccess extends CategoriesState {
  const CategoriesSuccess({required super.categoriesList});
}

class CategoriesFailed extends CategoriesState {}