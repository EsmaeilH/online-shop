import 'package:online_store/models/Categories_model.dart';

class ProductModel {
  ProductModel({
    this.id,
    this.title,
    this.price,
    this.description,
    this.images,
    this.category,
  });

  factory ProductModel.fromJson(Map<String, dynamic> json) {
    return ProductModel(
        id: json['id'],
        title: json['title'],
        price: json['price'],
        description: json['description'],
        images: json['images'] != null ? json['images'].cast<String>() : [],
        category: json['category'] != null
            ? CategoriesModel.fromJson(json['category'])
            : null);
  }

  int? id;
  String? title;
  int? price;
  String? description;
  List<String>? images;
  CategoriesModel? category;

  static List<ProductModel> productsFromSnapshot(List productSnapshot) {
    return productSnapshot.map((data) {
      return ProductModel.fromJson(data);
    }).toList();
  }
}
