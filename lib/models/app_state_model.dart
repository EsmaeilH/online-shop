import 'package:hive/hive.dart';

part 'app_state_model.g.dart';

@HiveType(typeId: 1)
class AppStateModel {
  @HiveField(0)
  final String theme;

  @HiveField(1)
  final String locale;

  @HiveField(2)
  final String accessToken;

  AppStateModel(
      {required this.theme, required this.locale, required this.accessToken});
}
