
class CategoriesModel {
  CategoriesModel({
    this.id,
    this.name,
    this.image,
  });

  factory CategoriesModel.fromJson(Map<String, dynamic> json) =>
      CategoriesModel(
          id: json['id'],
          name: json['name'],
          image: json['image']);

  int? id;
  String? name;
  String? image;

  static List<CategoriesModel> categoriesFromSnapshot(List categoriesSnapshot) {
    return categoriesSnapshot.map((data) {
      return CategoriesModel.fromJson(data);
    }).toList();
  }
}
