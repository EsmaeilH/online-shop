// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_state_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AppStateModelAdapter extends TypeAdapter<AppStateModel> {
  @override
  final int typeId = 1;

  @override
  AppStateModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AppStateModel(
      theme: fields[0] as String,
      locale: fields[1] as String,
      accessToken: fields[2] as String,
    );
  }

  @override
  void write(BinaryWriter writer, AppStateModel obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.theme)
      ..writeByte(1)
      ..write(obj.locale)
      ..writeByte(2)
      ..write(obj.accessToken);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppStateModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
