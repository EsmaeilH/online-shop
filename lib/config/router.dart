import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:online_store/blocs/app/app_bloc.dart';
import 'package:online_store/screens/categories_screen.dart';
import 'package:online_store/screens/error_screen.dart';
import 'package:online_store/screens/home_screen.dart';
import 'package:online_store/screens/login_screen.dart';
import 'package:online_store/screens/product_details.dart';
import 'package:online_store/screens/products_screen.dart';

import '../blocs/app/app_state.dart';

final router = GoRouter(
    initialLocation: '/',
    errorBuilder: (context, state) {
      return const ErrorScreen();
    },
    redirect: (BuildContext context, GoRouterState state) {
      final login =
          context.read<AppBloc>().state.loginState == AuthState.authenticated;
      if (login) {
        if (state.subloc == '/login') return '/';
      }
      return null;
    },
    routes: <RouteBase>[
      GoRoute(
          name: 'home',
          path: '/',
          builder: (BuildContext context, GoRouterState state) =>
              const HomePage(),
          routes: [
            GoRoute(
                name: 'categoriesPage',
                path: 'categories',
                builder: (BuildContext context, GoRouterState state) =>
                    const CategoriesPage()),
          ]),
      GoRoute(
          name: 'loginPage',
          path: '/login',
          builder: (BuildContext context, GoRouterState state) =>
              const LoginPage()),
      GoRoute(
          name: 'productsPage',
          path: '/products/:endpoint',
          builder: (BuildContext context, GoRouterState state) {
            return ProductsPage(endpoint: state.params['endpoint']!,);
          }),
      GoRoute(
          name: 'productDetailsPage',
          path: '/details/:id',
          builder: (BuildContext context, GoRouterState state) =>
              ProductDetailsPage(id: state.params['id']!)),
      GoRoute(
          name: 'errorPage',
          path: '/pageNotFound',
          builder: (BuildContext context, GoRouterState state) =>
              const ErrorScreen()),
    ]);
