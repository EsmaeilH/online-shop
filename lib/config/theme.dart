
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:online_store/utils/value_manager.dart';

import '../utils/custom_color.dart';

class CustomTheme {

  static ThemeData lightTheme = ThemeData(
    scaffoldBackgroundColor: lightScaffoldColor,
    primaryColor: lightCardColor,
    appBarTheme: AppBarTheme(
      iconTheme: IconThemeData(
        color: lightIconsColor,
      ),
      backgroundColor: lightScaffoldColor,
      systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarColor: Colors.black
      ),
      centerTitle: true,
      titleTextStyle: TextStyle(
          color: lightTextColor, fontSize: AppFontSize.fs22, fontWeight: FontWeight.bold),
      elevation: 0,
    ),
    iconTheme: IconThemeData(
      color: lightIconsColor,
    ),

    textSelectionTheme: const TextSelectionThemeData(
      cursorColor: Colors.black,
      selectionColor: Colors.blue,
    ),
    cardColor: lightCardColor,
    brightness: Brightness.light,
    colorScheme: ThemeData().colorScheme.copyWith(
      background: lightBackgroundColor,
      secondary: lightIconsColor,
      brightness: Brightness.light,
    ),
  );

  static ThemeData darkTheme =  ThemeData(
    brightness: Brightness.dark,
    appBarTheme: const AppBarTheme(
      systemOverlayStyle: SystemUiOverlayStyle(
        statusBarColor: Colors.black
      )
    )
  );

}