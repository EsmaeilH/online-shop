const String baseUrl = "api.escuelajs.co";

const String productsEndPoint = "products";
const String categoriesEndPoint = "categories";
const String authLoginEndPoint = "auth/login";
const String authProfileEndPoint = "auth/profile";

const String testImageLink = "https://i.ibb.co/vwB46Yq/shoes.png";