
class AppMargin {
  static const double m8 = 8.0;
  static const double m12 = 12.0;
  static const double m14 = 14.0;
  static const double m16 = 16.0;
  static const double m18 = 18.0;
  static const double m20 = 20.0;
}

class AppPadding {
  static const double p8 = 8.0;
  static const double p12 = 12.0;
  static const double p14 = 14.0;
  static const double p16 = 16.0;
  static const double p18 = 18.0;
  static const double p20 = 20.0;
}

class AppSize {
  static const double s4 = 4.0;
  static const double s8 = 8.0;
  static const double s10 = 10.0;
  static const double s12 = 12.0;
  static const double s14 = 14.0;
  static const double s16 = 16.0;
  static const double s18 = 18.0;
  static const double s20 = 20.0;
  static const double s50 = 50.0;
  static const double s100 = 100.0;
  static const double s150 = 150.0;
  static const double s200 = 200.0;
}

class AppFontSize {
  static const double fs18 = 18.0;
  static const double fs22 = 22.0;
  static const double fs24 = 24.0;
}

class AppDuration {
  static const int d100 = 100;
  static const int d200 = 200;
  static const int d300 = 300;
  static const int d400 = 400;
  static const int d500 = 500;
  static const int ds2 = 2;
}

const double webScreenSize = 500;
const double sideMenuSize = 250;