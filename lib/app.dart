import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:online_store/blocs/app/app_bloc.dart';
import 'package:online_store/l10n/l10n.dart';
import 'package:online_store/config/router.dart';
import 'package:online_store/config/theme.dart';

import 'blocs/app/app_state.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(builder: (context, state) {
      return MaterialApp.router(
        debugShowCheckedModeBanner: false,
        title: "onlineStore",
        theme: CustomTheme.lightTheme,
        darkTheme: CustomTheme.darkTheme,
        themeMode: state.themeState == ThemeState.system
            ? ThemeMode.system
            : state.themeState == ThemeState.light
                ? ThemeMode.light
                : state.themeState == ThemeState.dark
                    ? ThemeMode.dark
                    : ThemeMode.system,
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: AppLocalizations.supportedLocales,
        locale: Locale(state.localeState.name),
        routerConfig: router,
      );
    });
  }
}
