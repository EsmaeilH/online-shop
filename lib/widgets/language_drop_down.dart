
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:online_store/blocs/app/app_event.dart';
import 'package:online_store/blocs/app/app_state.dart';

import '../blocs/app/app_bloc.dart';

class LanguageDropDown extends StatelessWidget {
  const LanguageDropDown({super.key});

  @override
  Widget build(BuildContext context) {
    List<String> localeList = LocaleState.values.map((item) => item.name).toList();
    final localeState = context.select((AppBloc b) => b.state.localeState.name);
    return DropdownButton(
      items: localeList.map((String item) => DropdownMenuItem<String>(value: item,child: Text(item),)).toList(),
      value: localeState,
      onChanged: (value) { context.read<AppBloc>().add(LocaleChanged(locale: value!));},
      iconEnabledColor: Colors.lightBlueAccent,
    );
  }

}