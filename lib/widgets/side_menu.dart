import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:go_router/go_router.dart';
import 'package:online_store/l10n/l10n.dart';
import 'package:online_store/widgets/side_menu_item.dart';

import '../blocs/app/app_bloc.dart';
import '../blocs/app/app_event.dart';
import '../blocs/app/app_state.dart';
import '../services/database_handler.dart';
import '../utils/value_manager.dart';
import 'custom_icon.dart';

class SideMenu extends StatelessWidget {
  const SideMenu({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(AppPadding.p8),
      child: ListView(
        children: [
          const SizedBox(
            height: AppSize.s200,
            child: ProfileInfo(),
          ),
          const Divider(),
          Column(
            children: [
              SideMenuItem(
                icon: const CustomIcon(
                    ltrIcon: IconlyBold.arrowRight,
                    rtlIcon: IconlyBold.arrowLeft),
                itemName: context.l10n.home,
                routeName: 'home',
              ),
              SideMenuItem(
                icon: const CustomIcon(
                    ltrIcon: IconlyBold.arrowRight,
                    rtlIcon: IconlyBold.arrowLeft),
                itemName: context.l10n.categories, routeName: 'categoriesPage',
              ),
              SideMenuItem(
                icon: const CustomIcon(
                    ltrIcon: IconlyBold.arrowRight,
                    rtlIcon: IconlyBold.arrowLeft),
                itemName: context.l10n.about, routeName: 'home',
              ),
            ],
          )
        ],
      ),
    );
  }
}

class ProfileInfo extends StatelessWidget {
  const ProfileInfo({super.key});

  @override
  Widget build(BuildContext context) {
    final loginState = context.select((AppBloc b) => b.state.loginState);
    return loginState == AuthState.authenticated
        ? BlocBuilder<AppBloc, AppState>(
        buildWhen: (previous, current) => previous.model.runtimeType != current.model.runtimeType,
        builder: (context, state) {
          context.read<AppBloc>().add(FetchProfile(
              token: context.read<AppStateDao>().defaultModel.accessToken));
          return Column(
            children: [
              CircleAvatar(
                backgroundColor: Colors.orangeAccent,
                radius: 40,
                child: state.model != null
                    ? CircleAvatar(
                  backgroundImage: NetworkImage(state.model!.avatar),
                  radius: 37,
                )
                    : const CircleAvatar(
                  backgroundImage:
                  AssetImage('assets/images/avatar.png'),
                  radius: 37,
                ),
              ),
              const SizedBox(
                height: AppSize.s10,
              ),
              SizedBox(
                height: AppSize.s20,
                child: Text(
                  state.model != null ? state.model!.name : context.l10n.waiting,
                  style: const TextStyle(fontWeight: FontWeight.w600),
                ),
              ),
              const SizedBox(
                height: AppSize.s10,
              ),
              ElevatedButton(
                  onPressed: () {
                    context
                        .read<AppBloc>()
                        .add(AuthStateChanged(isLogin: false));
                  },
                  child: Text(context.l10n.logout),),
            ],
          );
        })
        : Center(
      child: ElevatedButton(
          onPressed: () {
            context.pushNamed('loginPage');
          },
          child: Text(context.l10n.login)),
    );
  }

}
