
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:online_store/blocs/app/app_bloc.dart';

class CustomIcon extends StatelessWidget {
  const CustomIcon({super.key, required this.ltrIcon, required this.rtlIcon});

  final IconData rtlIcon;
  final IconData ltrIcon;

  @override
  Widget build(BuildContext context) {
    String locale = context.select((AppBloc A) => A.state.localeState.name);

    if(locale == "fa") {
      return Icon(rtlIcon);
    }else {
      return Icon(ltrIcon);
    }
  }

}