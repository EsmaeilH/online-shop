
import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:go_router/go_router.dart';
import 'package:online_store/widgets/custom_icon.dart';

class SideMenuItem extends StatelessWidget {
  const SideMenuItem({super.key, required this.icon, required this.itemName, required this.routeName});

  final CustomIcon icon;
  final String itemName;
  final String routeName;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {context.goNamed(routeName);},
      child: ListTile(
        title: Text(itemName),
        trailing: const CustomIcon(
            ltrIcon: IconlyBold.arrowRight,
            rtlIcon: IconlyBold.arrowLeft),
      ),
    );
  }

}