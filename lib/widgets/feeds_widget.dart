import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:go_router/go_router.dart';
import 'package:online_store/blocs/app/app_bloc.dart';
import 'package:online_store/models/product_model.dart';
import 'package:online_store/utils/custom_color.dart';
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:provider/provider.dart';

class FeedsWidget extends StatelessWidget {
  const FeedsWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final productModelProvider = context.watch<ProductModel>();
    final theme = context.read<AppBloc>().state.themeState;

    Size size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: Material(
        borderRadius: BorderRadius.circular(8.0),
        color: Theme.of(context).cardColor,
        child: InkWell(
            borderRadius: BorderRadius.circular(8.0),
            onTap: () {
              context.pushNamed('productDetailsPage', params: {'id' : "${productModelProvider.id}"});
            },
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 5, right: 5, top: 8),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                            child: RichText(
                                text: TextSpan(
                                    text: "\$",
                                    style: const TextStyle(
                                        color: Color.fromRGBO(33, 150, 243, 1)),
                                    children: <TextSpan>[
                              TextSpan(
                                  text: "${productModelProvider.price}",
                                  style: TextStyle(
                                      color: theme.name ==
                                          'light' ? lightTextColor : theme.name == 'dark' ? darkTextColor
                                          : MediaQuery.maybeOf(context)?.platformBrightness == Brightness.light ? lightTextColor : darkTextColor,
                                      fontWeight: FontWeight.w600))
                            ]))),
                        const Icon(IconlyBold.heart)
                      ]),
                ),
                const SizedBox(
                  height: 10,
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(12),
                  child: AspectRatio(
                    aspectRatio: 1.2,
                    child: FancyShimmerImage(
                      height: size.height * 0.2,
                      // width: double.infinity,
                      errorWidget: const Icon(
                        IconlyBold.danger,
                        color: Colors.red,
                        size: 28,
                      ),
                      imageUrl: productModelProvider.images![0],
                      boxFit: BoxFit.fill,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                    padding: const EdgeInsets.all(8),
                    child: Text(
                      productModelProvider.title.toString(),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: const TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w700
                      ),
                    )),
              ],
            )),
      ),
    );
  }
}
