
import 'package:hive/hive.dart';
import 'package:online_store/models/app_state_model.dart';

import '../blocs/app/app_state.dart';

class AppStateDao {
  static const boxKey = 'appState';

  AppStateModel _defaultModel = AppStateModel(
      theme: ThemeState.system.name, locale: LocaleState.en.name, accessToken: ''
  );

  AppStateModel get defaultModel => _defaultModel;

  AppStateDao({required this.dataBox}) {
    _getData();
  }

  final Box dataBox;

  Future<AppStateModel> _getData() async{
    if(dataBox.containsKey(boxKey)) {
      return _defaultModel = dataBox.get(boxKey);
    }else {
      return _defaultModel;
    }
  }

  void update(AppStateModel appStateModel) async{
    _defaultModel = appStateModel;
    dataBox.put(boxKey, appStateModel);
  }

}