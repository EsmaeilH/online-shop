import 'dart:convert';
import 'dart:developer';

import 'package:online_store/models/product_model.dart';
import 'package:http/http.dart' as http;

import '../models/Categories_model.dart';
import '../models/user_model.dart';
import '../utils/api_consts.dart';

class APIHandler {
  static Future<List<dynamic>> getData(
      {required String target, String? limit}) async {
    try {
      var uri = Uri.https(
          baseUrl,
          "api/v1/$target",
          target == productsEndPoint
              ? {
                  "offset": "0",
                  "limit": limit,
                }
              : {});
      var response = await http.get(uri).timeout(const Duration(seconds: 10), onTimeout: () {
        throw "Request Timeout";
      });

      var data = jsonDecode(response.body);
      List tempList = [];
      if (response.statusCode != 200) {
        throw data["message"];
      }
      for (var v in data) {
        tempList.add(v);
      }
      return tempList;
    } catch (error) {
      log("An error occurred $error");
      throw error.toString();
    }
  }

  static Future<List<ProductModel>> getAllProducts(
      {required String limit}) async {
    List temp = await getData(
      target: productsEndPoint,
      limit: limit,
    );
    return ProductModel.productsFromSnapshot(temp);
  }

  static Future<List<CategoriesModel>> getAllCategories() async {
    List temp = await getData(target: categoriesEndPoint);
    return CategoriesModel.categoriesFromSnapshot(temp);
  }

  static Future<ProductModel> getProductById({required String id}) async {
    try {
      var uri = Uri.https(
        baseUrl,
        "api/v1/$productsEndPoint/$id",
      );
      var response = await http.get(uri);

      var data = jsonDecode(response.body);
      if (response.statusCode != 200) {
        throw data["message"];
      }
      return ProductModel.fromJson(data);
    } catch (error) {
      log("an error occurred while getting product info $error");
      throw error.toString();
    }
  }

  static Future<dynamic> login(
      {required String email, required String password}) async {
    var body = {"email": email, "password": password};
    try {
      var uri = Uri.https(baseUrl, "api/v1/$authLoginEndPoint", body);
      var response = await http.post(uri);

      var data = jsonDecode(response.body);
      if (response.statusCode != 201) {
        throw data["message"];
      }

      return data;
    } catch (error) {
      log("an error occurred while getting login info : $error");
      throw error.toString();
    }
  }

  static Future<UserModel> getUserProfile({required String token}) async {
    try {
      var header = {"Authorization": "Bearer $token"};
      var uri = Uri.https(
        baseUrl,
        "api/v1/$authProfileEndPoint",
      );
      var response = await http.get(uri, headers: header);

      var data = jsonDecode(response.body);
      if (response.statusCode != 200) {
        throw data["message"];
      }

      return UserModel.fromJson(data);
    } catch (error) {
      log("an error occurred while getting login info : $error");
      throw error.toString();
    }
  }
}
