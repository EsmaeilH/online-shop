# online_store

![alt text](assets/screenshot/web_1.png)
![alt text](assets/screenshot/web_2.png)
![alt text](assets/screenshot/mobile_1.png)
![alt text](assets/screenshot/mobile_2.png)
![alt text](assets/screenshot/mobile_3.png)

## Description

It's a ecommerce app that uses a restapi service to connect to the database and fetch product list and details from it and show it to the user.

- multiple platform (mobile - desktop - web)
- jwt token for login
- multiple languages
- multiple themes

## StateManager

- Bloc and Cubit

## Packages

- http
- bloc
- hive
- go_router
